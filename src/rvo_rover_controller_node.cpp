/*
Author: Shih-Yuan Liu
*/

#include <ros/ros.h>
#include <vector>
#include <map>
#include <sstream>
#include <algorithm>

#include <rvo_ros/Agent.h>
#include <rvo_ros/AgentParam.h>
#include <rvo_ros/AgentState.h>
#include <rvo_ros/AgentInfo.h>
#include <rvo_ros/Obstacles.h>
#include <rvo_ros/Crowd.h>
#include <rvo_ros/VoInfo.h>

#include <rvo_ros/AddAgentState.h>
#include <rvo_ros/UpdateAgentState.h>
#include <rvo_ros/RemoveAgentId.h>
#include <rvo_ros/AddAgent.h>
#include <rvo_ros/SetObstacles.h>
#include <std_srvs/Empty.h>
#include <CrowdSimulator.hpp>
#include <RvoParamLoader.hpp>
#include "tf/transform_datatypes.h"

#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>

#include <ford_msgs/Clusters.h>
#include <ford_msgs/Pose2DStamped.h>


class RVORoverControllerNode{
public:
  ros::NodeHandle nh_p_;
  CrowdSimulator sim_;
  RvoParamLoader param_loader_;
  std::string node_name_;

  /* Publisher */
  ros::Publisher pub_crowd_;
  ros::Publisher pub_obs_;
  ros::Publisher pub_vel_;

  /* Subscriber */
  ros::Subscriber sub_clusters_;
  ros::Subscriber sub_rover_;
  ros::Subscriber sub_desired_vel_;

  /* Srv */
  ros::ServiceServer srv_add_state_;
  ros::ServiceServer srv_add_agent_;
  ros::ServiceServer srv_update_state_;
  ros::ServiceServer srv_set_obstacles_;
  ros::ServiceServer srv_remove_;
  ros::ServiceServer srv_start_;
  ros::ServiceServer srv_pause_;

  /* Sim Parameters */
  float timeStep_;
  /* Default Agent Parameters*/
  float neighborDist_;
  int maxNeighbors_;
  float timeHorizon_;
  float timeHorizonObst_;
  float radius_;
  float maxSpeed_;
  float maxAcc_;
  float lambdaValue_;
  RVO::Vector2 velocity_;

  std::string frame_id_;

  geometry_msgs::Vector3 desired_vel_;

  RVORoverControllerNode(const ros::NodeHandle& nh):nh_p_(nh),param_loader_(nh)
  {
    setDefaultParams();
    getParams();
    node_name_ = ros::this_node::getName();
    sim_.setAgentDefaults(getDefaultAgentParam());
    sim_.setTimeStep(timeStep_); // Doesn't matter 

    /* handle obstacles */
    sim_.setObstacles(rvo_ros::Obstacles());

    /* Advertise Services */
    // srv_add_state_ = nh_p_.advertiseService("add_agent_state", &RVORoverControllerNode::cbAddAgentState, this);
    // srv_add_agent_ = nh_p_.advertiseService("add_agent", &RVORoverControllerNode::cbAddAgent, this);
    // srv_update_state_ = nh_p_.advertiseService("update_agent_state", &RVORoverControllerNode::cbUpdateAgentState, this);
    // srv_remove_ = nh_p_.advertiseService("remove_agent_id", &RVORoverControllerNode::cbRemoveAgentId, this);
    srv_set_obstacles_ = nh_p_.advertiseService("set_obstacles", &RVORoverControllerNode::cbSetObstacles, this);
    // srv_start_ = nh_p_.advertiseService("start", &RVORoverControllerNode::cbStart, this);
    // srv_pause_ = nh_p_.advertiseService("pause", &RVORoverControllerNode::cbPause, this);
    
    /* Advertise obstacles */
    pub_crowd_ = nh_p_.advertise<rvo_ros::Crowd>("crowd",1,true);
    pub_obs_ = nh_p_.advertise<rvo_ros::Obstacles>("obstacles",1,true);
    pub_vel_ = nh_p_.advertise<geometry_msgs::Vector3>("velocity",1,true);

    /* Subscribers */
    sub_clusters_ = nh_p_.subscribe("clusters",1,&RVORoverControllerNode::cbClusters, this);
    sub_rover_ = nh_p_.subscribe("rover_pose",1,&RVORoverControllerNode::cbRover, this);
    sub_desired_vel_ = nh_p_.subscribe("desired_vel",1,&RVORoverControllerNode::cbDesiredVel, this);
  }
  ~RVORoverControllerNode(){}

  rvo_ros::AgentParam getDefaultAgentParam(){
    rvo_ros::AgentParam param;
    param_loader_.getParam("PD",param);
    return param;
  }

  void setDefaultParams()
  {
    // if (!ros::param::has("~timeStep")) { ros::param::set("~timeStep",0.02);}
    if (!ros::param::has("~frame_id")) { ros::param::set("~frame_id","/vicon");}
  }
  void getParams()
  {
    /*TODO: read parameters*/
    param_loader_.loadVehParam("PD");
    ros::param::getCached("~timeStep",timeStep_);
    ros::param::getCached("~frame_id",frame_id_);
  }

  bool cbSetObstacles(rvo_ros::SetObstacles::Request& request, rvo_ros::SetObstacles::Response& response)
  {
    rvo_ros::Obstacles obs_msg;
    obs_msg.obstacles = request.obstacles;
    sim_.setObstacles(obs_msg);
    pub_obs_.publish(sim_.getObstacles());
    /* Publish obstacle msg when it's set */
    return true;
  }

  void publishCrowd()
  {
    // ROS_INFO_STREAM("[publishCrowd]");
    rvo_ros::Crowd crowd = sim_.getCrowd();
    crowd.header.frame_id = frame_id_;
    // crowd.header.stamp = timerEvent.current_expected;
    // crowd.header.stamp = time_init_ + ros::Duration(sim_.getGlobalTime());
    crowd.header.stamp = ros::Time::now();
    pub_crowd_.publish(crowd);    
  }


  void cbDesiredVel(const geometry_msgs::Vector3 desired_vel_msg)
  {
    desired_vel_.x = desired_vel_msg.x;
    desired_vel_.y = desired_vel_msg.y;
  }

  void cbRover(const ford_msgs::Pose2DStamped& rover_msg)
  {
    /* Insert host agent */
    rvo_ros::Agent agent_msg;
    agent_msg.id = 0;
    agent_msg.param = getDefaultAgentParam();
    /* TODO: Set host agent param */

    /* Set agent state*/
    agent_msg.state.pos.x = rover_msg.pose.x;
    agent_msg.state.pos.y = rover_msg.pose.y;
    agent_msg.state.vel.x = rover_msg.velocity.x;
    agent_msg.state.vel.y = rover_msg.velocity.y;
    agent_msg.state.prefVelocity = desired_vel_;

    /* Add agent to sim */
    sim_.addAgent(agent_msg);

    /* Solve for safe velocity */
    RVO::Agent* host_agent_ptr = sim_.getAgent(0);
    sim_.computeNewVelocity(host_agent_ptr);

    /* Publish the RVO velocity of the rover */
    geometry_msgs::Vector3 vel_msg;
    vel_msg.x = velocity_.x();
    vel_msg.y = velocity_.y();
    pub_vel_.publish(vel_msg);

    /* Publish crowd for visualization*/
    publishCrowd();
  }

  void cbClusters(const ford_msgs::Clusters& clusters_msg)
  {
    // TODO: initialize for the first time
    sim_.clearAgents();
    /* Extract agent from clusters and update the sim */
    for (int i = 0; i < clusters_msg.labels.size(); ++i){
      rvo_ros::Agent agent_msg;
      agent_msg.id = clusters_msg.labels[i] + 1; // agent.id = 0 preserved for the rover
      agent_msg.param = getDefaultAgentParam();
      agent_msg.state.pos = clusters_msg.mean_points[i];
      agent_msg.state.vel = clusters_msg.velocities[i];
      agent_msg.state.prefVelocity = clusters_msg.velocities[i];
      /* Compute radius */      
      RVO::Vector2 to_max(clusters_msg.max_points[i].x - clusters_msg.mean_points[i].x,clusters_msg.max_points[i].y - clusters_msg.mean_points[i].y);
      RVO::Vector2 to_min(clusters_msg.min_points[i].x - clusters_msg.mean_points[i].x,clusters_msg.min_points[i].y - clusters_msg.mean_points[i].y);
      // agent_msg.param.radius = math::max(RVO::abs(to_max),RVO::abs(to_min));
      agent_msg.param.radius = RVO::abs(to_max) > RVO::abs(to_min) ? RVO::abs(to_max) : RVO::abs(to_min);

      sim_.addAgent(agent_msg);
    }
  }

  // void cbStep(const ros::TimerEvent& timerEvent)
  // {
  //   // updateSimByCurrentState();
  //   sim_.computeNewVelocity();
  //   // publishRvo();
  //   sim_.moveAgents();
  //   publishCrowd();
  // }

  // bool cbAddAgent(rvo_ros::AddAgent::Request& request, rvo_ros::AddAgent::Response& response)
  // {
  //   sim_.addAgent(request.agent);
  //   response.id = request.agent.id;
  //   // ROS_INFO_STREAM("[CrowdNode]: Add agent " << request.agent.id);
  //   ROS_INFO("[%s]: Add agent %d", node_name_.c_str(), request.agent.id);
  //   publishCrowd();
  //   return true;
  // }

  // bool cbUpdateAgentState(rvo_ros::UpdateAgentState::Request& request, rvo_ros::UpdateAgentState::Response& response){
  //   // ROS_INFO_STREAM("[CrowdNode] UpdateAgentState called from agent " << request.id );
  //   RVO::Agent* agent = sim_.getAgent(request.id);
  //   if (agent == NULL){
  //     response.has_agent = false;
  //   }
  //   else{
  //     response.has_agent = true;
  //     sim_.updateAgentState(agent,request.state,request.update_pos,request.update_vel,request.update_prefVel, request.update_goals);
  //     rvo_ros::Agent agent_msg = sim_.toAgentMsg(*agent);
  //     response.state = agent_msg.state;
  //   }
  //   // publishCrowd();
  //   return true;
  // }

  // bool cbAddAgentState(rvo_ros::AddAgentState::Request& request, rvo_ros::AddAgentState::Response& response)
  // {
  //   rvo_ros::Agent agentMsg;
  //   agentMsg.id = sim_.max_id_;
  //   agentMsg.state = request.state;
  //   agentMsg.param = getDefaultAgentParam();
  //   sim_.addAgent(agentMsg);
  //   response.id = agentMsg.id;
  //   ROS_INFO("[%s]: Add agent %d ",node_name_.c_str(),agentMsg.id);
  //   publishCrowd();
  //   return true;
  // }  

  // bool cbRemoveAgentId(rvo_ros::RemoveAgentId::Request& request, rvo_ros::RemoveAgentId::Response& response)
  // {
  //   response.flag = sim_.removeAgent(request.id);
  //   if (response.flag){
  //     // ROS_INFO_STREAM("[CrowdNode]: Remove Agent :" << request.id);
  //     ROS_INFO("[%s]: Remove agent %d ",node_name_.c_str(),request.id);

  //   }
  //   else{
  //     // ROS_INFO_STREAM("[CrowdNode]: Agent " << request.id << " does not exist.");
  //   }
  //   publishCrowd();
  //   return true;
  // }

  // bool cbStart(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  // {
  //   timer_sim_.start();
  //   return true;
  // }

  // bool cbPause(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  // {
  //   timer_sim_.stop();
  //   return true;
  // }
};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rvo_rover_controller_node");
  ros::NodeHandle nh("~");

  RVORoverControllerNode node(nh);
  ros::spin();
  return 0;
}